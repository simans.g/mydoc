# MyDoc Backend

## Overview

This page consists coding challenge for Backend Engineer role at MyDoc 

## NPM Library notes

- [Objection.js](https://vincit.github.io/objection.js/) is used as the ORM. It works on top of the [Knex](https://knexjs.org/) SQL Query builder.
  - It is best that knex be installed globally (`npm i -g knex`)
- ESLint should be set up for the project.
- These extensions should be set up in VSCode:
  - [ESLint](https://github.com/Microsoft/vscode-eslint)

## Usage

### Database setup

- `knex migrate:latest` to create the database structures
- `knex seed:run` to seed the database (e.g. with supplier information)

### Environment variables setup

- Please refer to `.env.schema` for environment vars that need to be set.
- Set `DB_DEBUG=true` to log all DB requests

### Express

- `npm run dev` to build and start the API server
- `npm run test` to run unit test 

## Conventions and Design

See [DESIGN.md](DESIGN.md)

