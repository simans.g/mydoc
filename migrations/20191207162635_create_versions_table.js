exports.up = function (knex, _) {
  return knex.schema
    .createTable('versions', table => {
      table.increments('id').primary()
      table.string('key').notNullable()
      table.text('value').notNullable()

      table.dateTime('updatedAt').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))

      table.index('id')
    })
}

exports.down = function (knex, _) {
  return knex.schema
    .dropTable('versions')
}
