exports.up = function (knex, _) {
  return knex.schema
    .createTable('version_history', table => {
      table.increments('id').primary()
      table.integer('versionId')
      table.text('value').notNullable()

      table.dateTime('createdAt').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))

      table.index('versionId')
    })
}

exports.down = function (knex, _) {
  return knex.schema
    .dropTable('version_history')
}
