# Design document

## Overview

This document recaps the reasoning behind the API architecture, feedback is welcome

## SQL vs NoSQL
A hybrid approach will be used here:
* MySQL as the main backing store for all these. 

## ORM vs No-ORM
* ORMs: https://en.wikipedia.org/wiki/Object-relational_mapping
* The main drawback of ORMs is that it abstracts away DB performance, this is something that can be addressed through reviews.
  * That said, because ORMs make [Join decomposition](https://www.oreilly.com/library/view/high-performance-mysql/9780596101718/ch04.html) easier, which avoids massive JOINs, ORMs can offer good performance if planned properly.
* The main benefit is something that is needed now - increased productivity as it reduces the amount of code needed.
* [Objection.js](http://vincit.github.io/objection.js/) will be used. Objection.js is an ORM that builds on top of Knex, an SQL builder library

## 12-factor

## Environment variables
`dotenv` is the standard for this. We are using a variant `dotenv-extended`. This follows a standard where:
* Any environment variables defined in the container/VM take FIRST priority
* A `.env` file will take second priority (useful for local dev)
* Any new env variables should be in `.env.schema` with a default value in `.env.defaults`

## Code standard
ESLint "standard" is used

## Code layout

### Top-level
```
/node_modules   
/migrations           //migrations and seeds help maintain changes in the DB state. see https://knexjs.org/#Installation-migrations
/seeds    
/src                  //See "Main Codebase" below
.eslintrc.json        //configures ESLint for the project
knexfile.js           //DB config 
```
### Main codebase
```
/controllers      // See "Controllers, routes, and middleware" section
/gateways         // See "Model Gateways" section
/models           // See "Models"
/routes           //
|- /middleware    // See "Controllers, routes, and middleware" section
/transformers     // See "Transformers"
/utils            // See "Utils"
bootstrap.js      
server.js
```
#### Controllers, routes and middleware

* **Controllers** should parse the incoming request, direct it to a model/service/gateway that performs operations, then return a formatted response.
  * **Skinny Controllers** See [Skinny Controllers, Fat Services](https://medium.com/marmolabs/skinny-models-skinny-controllers-fat-services-e04cfe2d6ae). A Controller should do the least work possible. This is because controllers are rarely reusable.
  Among the things a controller should ***not*** handle:
    * **Authentication**. This should be done by middleware.
    * **Calls to external services**. This should be done by a dedicated Service.
    * **Complex DB operations**. This should be done by a model's Gateway.
    * **Formatting responses**. This should be done by a response transformer.
  * **Resource conventions**. Most Controllers should represent a Resource. Use this convention for these common controller operations (influenced by Laravel and Rails):
    * **index**: List a resource
    * **show**: Show a single resource
    * **store**: Create a resource
    * **update**: Update a resource
* **Routes** should present a concise summary of paths available in the app. There should be NO other work done in a route definition other than routing and middleware assignment.
* **Middleware** should be used to handle common operations that take place before and after a controller. These include:
  * Token parsing and authentication
  * Global request/error logging
  * Authorization for certain resources.

#### Error/Exception handling
Please register exceptions in the `errors` folder, and throw specific errors when you catch exceptions. This is so they can be identified quickly.

#### Models (Database and non-Database)
* **Database Models** Refer to [Objection.js](http://vincit.github.io/objection.js/). 
  * Every entry in `models` should represent a database table.
  * All user operations should be done through these models. This promotes reusability and less code written.
  * DON'T do complex or cross-model queries in here, use Gateways instead

#### Model Gateways
Sometimes known as a "Repository" pattern. Complex operations on database models should be done in a corresponding model Gateway.

#### Transformers
* See [Datran documentation](https://github.com/chalcedonyt/datran). Transformers should format incoming objects (usually DB models) before they are returned to the user.
  * DON'T do processing in here, the only operations should be things like loading related DB models.
  * Transformers can be chained and multiple versions can be done to cater for different use-cases. E.g. admin vs public views of the same model.

#### Utils
* Don't overuse utils unless `lodash` or `moment` can't handle what you need to do.
* A good use-case is configuration of major libraries like Redis, DB, Firebase.