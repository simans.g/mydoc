require('dotenv-extended').load()
module.exports = {
  debug: process.env.DB_DEBUG ? /^true$/i.test(process.env.DB_DEBUG) : false,
  client: 'mysql',
  useNullAsDefault: true,
  connection: {
    host: process.env.DB_HOST,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    port: process.env.DB_PORT || 3306,
    timezone: 'UTC',
    multipleStatements: process.env.KNEX_MULTIPLE_STATEMENTS ? /^true$/i.test(process.env.KNEX_MULTIPLE_STATEMENTS) : false
  },
  pool: {
    min: parseInt(process.env.DB_POOL_MIN) || 5,
    max: parseInt(process.env.DB_POOL_MAX) || 75
  }
}
