require('./bootstrap')
const express = require('express')
const app = express()
const cors = require('cors')
const bodyParser = require('body-parser')
const path = require('path')


const port = process.env.NODE_PORT || 8080;
app.listen(port,() => console.log('MyDoc  listening on port ::' + port));


app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

// catch bodyParser errors
app.use((error, req, res, next) => {
  if (error instanceof SyntaxError) {
    res.status(400)
    res.json({
      error: 'MalformedJSON',
      message: `The server could not parse your JSON request: ${error.message}`
    })
  } else {
    next()
  }
})

const { publicRouter} = require('./src/routes')

app.use(`/${0.1}/`, publicRouter)

app.use(express.static('public'))
