const { Model } = require('objection')

module.exports = class VersionHistory extends Model {
  static get tableName () {
    return 'version_history'
  }
}