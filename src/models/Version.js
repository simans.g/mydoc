const { Model } = require('objection')

module.exports = class Version extends Model {
  static get tableName () {
    return 'versions'
  }

  $beforeUpdate () {
    this.updatedAt = new Date().toISOString().slice(0, 19).replace('T', ' ')
  }
  $beforeInsert () {
    this.updatedAt = new Date().toISOString().slice(0, 19).replace('T', ' ')
  }

  static get relationMappings () {
    const VersionHistory = require('./VersionHistory')
    return {
      versionHistory: {
        relation: Model.HasManyRelation,
        modelClass: VersionHistory,
        join: {
          from: 'version_history.versionId',
          to: 'versions.id'
        }
      }
    }
  }
}