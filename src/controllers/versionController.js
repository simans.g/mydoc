const datran = require('datran')
const { Version } = require('../models')

const { BaseVersionTransformer } = require('../transformers')

const VersionGateway = require('../gateways/VersionGateway')


/**
 * @todo need 404 and error handling
 * @param {Request} req
 */
const show = async (req) => {
    const { mykey } = req.params
    const timestamp = req.query.timestamp

    if (!mykey) {
        throw new error('there should be key to serach')
    }
    const version = await VersionGateway.show(mykey, timestamp)
    let response = {}
    if (version) {
        response = await datran.create(
            datran.item(version, new BaseVersionTransformer())
        ).toObject()
        delete response.key
        delete response.timestamp
    }


    return response
}

/**
 * this method is to store data
 * @param {*} req 
 */
const store = async (req) => {
    console.log(`store method called`)
    let items = Object.entries(req.body)
    if (items.length > 1) {
        throw console.error()
    }
    let [request] = items
    const data = await VersionGateway.create(request[0], request[1])
    return datran.create(
        datran.item(data, new BaseVersionTransformer())
    ).toObject()
}


module.exports = {
    show,
    store
}
