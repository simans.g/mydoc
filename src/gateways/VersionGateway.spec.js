require('../tests/test-bootstrap')
const { expect } = require('chai')
const sandbox = require('sinon').createSandbox()
const { Version } = require('../models')
const VersionGateway = require('./VersionGateway')

describe('VersionGateway', () => {
    it('Create New Key Value', async () => {
        const randomKey = Math.random().toString(8).substr(2, 1).toUpperCase()
        const keyValueCreated = await VersionGateway.create(randomKey, 'testValue')
        expect(keyValueCreated).is.not.null
        expect(keyValueCreated.key).is.eqls(randomKey)
    })

    it('Get Value By Key', async () => {
        const keyValueCreated = await VersionGateway.show('test', null)
        expect(keyValueCreated).is.not.null
        expect(keyValueCreated.key).is.eqls('test')
    })

    it('search by time', async () => {
        const keyValueCreated = await VersionGateway.show('mykey', '1575989654')
        expect(keyValueCreated).is.not.null
        expect(keyValueCreated.key).is.eqls('mykey')
        expect(keyValueCreated.timestamp).is.eqls(1575989654)
    })
})

