const { Version, VersionHistory } = require('../models')
const moment = require('moment')
module.exports = class VersionGateway {

    /**
     * Purpose is to get key value pair by key.
     * @param {sting} key 
     * @param {unix | null} timeStamp 
     */
    static async show(key, timeStamp) {
        let version
        if (!timeStamp) {
            version = await Version.query().findOne({ key })
        } else {
            version = await VersionGateway.filter(key, timeStamp)
        }
        return version
    }
    /**
     * Purpose of this method is to create version record in the database.
     * @param {string} key 
     * @param {text} value 
     */
    static async create(key, value) {
        const existing = await Version.query().findOne({ key })
        if (!existing) {
            await Version
                .query()
                .insert({ 'key': key, 'value': value })
        } else {
            await VersionHistory
                .query()
                .insert({ 'versionId': existing.id, 'value': existing.value, 'createdAt': existing.updatedAt })

            await Version.query()
                .where('key', '=', key)
                .update({ 'value': value })
        }

        const savedData = await Version.query().findOne({ key })
        return savedData
    }


    /**
     * Purpose of this method filter versions
     * @param {string} key 
     * @param {unix time} timeStamp 
     */
    static async filter(key, timeStamp) {
        const version = await Version.query().findOne({ key })

        if (!version) {
            return []
        }
        await version.$fetchGraph('versionHistory')

        const allVersions = []
        allVersions.push({ 'id': version.id, 'key': version.key, 'value': version.value, 'timestamp': moment(version.updatedAt).unix(), 'updatedAt': version.updatedAt })
        if (Array.isArray(version.versionHistory) && version.versionHistory.length > 0) {
            version.versionHistory.forEach(element => {
                allVersions.push({ 'id': version.id, 'key': version.key, 'value': element.value, 'timestamp': moment(element.createdAt).unix(), 'updatedAt': element.createdAt })
            });
        }

        var filterValue = moment.unix(timeStamp);

        const data = allVersions.filter(element => {
            return moment(element.updatedAt).isSameOrBefore(filterValue)
        })

        data.sort((a, b) => { return a.timestamp < b.timestamp })

        return data.length ? data[0] : undefined
    }
}