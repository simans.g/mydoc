const moment = require('moment')
const { Transformer } = require('datran')

module.exports = class BaseVersionTransformer extends Transformer {
  async transform (o) {
    const {
      key,
      value,
      updatedAt
    } = o

    return {
      key,
      value,
      timestamp : this.getTimestamp(updatedAt)
    }
  }

  getTimestamp(updatedAt){
    const timestamp = new moment(updatedAt).format("hh:mm a");
    return timestamp
  }
}
