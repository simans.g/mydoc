require('dotenv-extended').load()
module.exports = {
  debug: process.env.DB_DEBUG ? /^true$/i.test(process.env.DB_DEBUG) : false,
  client: 'mysql',
  useNullAsDefault: true,
  connection: {
    host: process.env.DB_HOST,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    port: process.env.DB_PORT || 3306,
    timezone: 'UTC',
    multipleStatements: true
  },
  // needs to be 1 so that tests can be wrapped in a single transaction reliably
  pool: {
    min: 1,
    max: 1
  }
}
