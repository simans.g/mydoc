/**
 * @todo Put in 204 for deletes
 * @param {Request} req
 * @param {Resource} res
 * @param {string} controllerMethod
 */
const jsonResponse = async (req, res, controllerMethod) => 
{
  try {
    const response = await controllerMethod(req, res)
    // if intentionally returning empty object or boolean true, then put a 204 status
    if (response === {} || response === true) {
      res.status(204)
      res.json({})
    } else {
      res.status(200)
      res.json(response)
    }
  } catch (err) {
    responseFromError(res, err)
  }
}

const responseFromError = (res, err) => {
    const privateMessage = err.sql && err.sqlMessage
      ? 'An internal error has occurred. This has been logged. Please try again later'
      : err.message
    const publicMessage = err.publicMessage || ''
    const errorStatus = err.status || 500
    const additionalProps = err.additionalProps && Object.keys(err.additionalProps).length
      ? err.additionalProps
      : {}
  
   
    res.status(errorStatus)
    res.json({
      ...additionalProps,
      error: err.constructor.name,
      message: privateMessage,
      publicMessage,
      key: err.key
    })
    return res
  }


module.exports = {
  jsonResponse
}
