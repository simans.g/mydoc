const router = require('express').Router()
const cors = require('cors')
const {
  versionController
} = require('../controllers')

const { jsonResponse } = require('./shared')

const middleware = []

router.use(cors())

router.post('/object', middleware, (req, res) => jsonResponse(req, res, versionController.store))
router.get('/object/:mykey', middleware, (req, res) => jsonResponse(req, res, versionController.show))

module.exports = router
