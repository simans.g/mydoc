
exports.seed = function (knex, Promise) {
    return knex('version_history').del()
      .then(function () {
        return knex('version_history').insert([
          {
            id: 1,
            versionId: 1,
            value: 'value1',
            createdAt: '2019-12-08 10:04:41'
          },
          {
            id: 2,
            versionId: 2,
            value: 'simans',
            createdAt: '2019-12-08 10:08:06'
          },
          {
            id: 3,
            versionId: 2,
            value: 'simans gunaseelan',
            createdAt: '2019-12-08 10:49:58'
          }
        ])
      })
  }
  