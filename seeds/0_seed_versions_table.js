
exports.seed = function (knex, Promise) {
    return knex('versions').del()
      .then(function () {
        return knex('versions').insert([
          {
            id: 1,
            key: 'value',
            value: 'value2',
            updatedAt: '2019-12-08 10:06:20'
          },
          {
            id: 2,
            key: 'test',
            value: 'simans g',
            updatedAt: '2019-12-08 14:59:05'
          }
        ])
      })
  }
  