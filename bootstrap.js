require('dotenv-extended').load({
  errorOnMissing: true,
  includeProcessEnv: true
})

const db = require('./src/utils/db')
const { Model } = require('objection')
Model.knex(db)